""" @package pypress
@brief This package contains a simple Tree container object.
@date 2017-07-01 Wed 05:21 AM
@author Sangsoic <sangsoic@protonmail.com>
@copyright
Copyright 2017 Sangsoic library author
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

             http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from collections.abc import Iterable;

class Tree(object):
	"""
	Tree() -> create new empty Tree node
	
	Build a Tree collection of data.
	"""

        ## @fn def walk(cls, tree=None, path_recorder=None)
        # @brief Walks through the nodes of a given tree and potentially record the path for each node.
        # @author Sangsoic <sangsoic@protonmail.com>
        # @version 0.1
        # @date 2017-07-01 Wed 05:16 PM
        # @param tree A given \a Tree object instance.
        # @param path_recorder Empty list use to record the path of each node.
        # @return A generator composed of either each node in the given tree or the path of each node in the given tree.
        # This function is a generator and a class function.
	def walk(cls, tree=None, path_recorder=None):
		if isinstance(tree, Tree):
			if isinstance(path_recorder, list):
				path_recorder.append(tree);
				yield path_recorder.copy();
			else:
				yield tree;
			childs = tree.get();
			if not childs is None:
				for child in childs:
					yield from cls.walk(tree=child, path_recorder=path_recorder);
			if isinstance(path_recorder, list):
				if path_recorder:
					del path_recorder[-1];
	walk = classmethod(walk);

        ## @fn def exists(cls, tree, setfi_paths=None, setfo_paths=None)
        # @brief Tests if all given paths exists within a given tree.
        # @author Sangsoic <sangsoic@protonmail.com>
        # @version 0.1
        # @date 2017-07-01 Wed 09:16 PM
        # @param cls Class address.
        # @param tree A given \a Tree object instance.
        # @param setfi_paths List containing values corresponding to node data.
        # @param setfo_paths List containing \a Tree object instance.
        # @return True if all paths exist else False.
        # This function is a class function.
	def exists(cls, tree, setfi_paths=None, setfo_paths=None):
		def match(cwn, path, flag):
			if flag:
				match_status = cwn.data == path[0];
			else:
				match_status = cwn is path[0];
			if not match_status:
				return match_status;
			cwn_childs = cwn.get();
			del path[0];
			if not path:
				return match_status;
			if not cwn_childs is None:
				for child in cwn_childs:
					match_status = match(cwn=child, path=path, flag=flag);
					if match_status:
						break;
			else:
				match_status = False;
				return match_status;
			return match_status;
		if not isinstance(tree, Tree):
			return False;
		if isinstance(setfi_paths, list):
			setfi_paths = [path for path in setfi_paths if isinstance(path, list) if path];
			for path in setfi_paths:
				if not match(cwn=tree, path=path, flag=True):
					return False;
		if isinstance(setfo_paths, list):
			setfo_paths = [[tree for tree in path if isinstance(tree, Tree)] for path in setfo_paths if isinstance(path, list) if path];
			for path in setfo_paths:
				if not match(cwn=tree, path=path, flag=False):
					return False;
		return True;
	exists = classmethod(exists);

        ## @fn def treed(cls, setfi_paths=None, setfo_paths=None)
        # @brief Converts a list of linear paths to a \a Tree object instance.
        # @author Sangsoic <sangsoic@protonmail.com>
        # @version 0.1
        # @date 2017-07-01 Wed 11:11 PM
        # @param cls Class address.
        # @param setfi_paths List containing values corresponding to node data.
        # @param setfo_paths List containing \a Tree object instance.
        # @return Newly allocated set of trees.
        # This function a class function.
        # A linear path is a implemented as a list of value, where each value corresponds to the data of a node (setfi list) or a node (setfo list).
        # Each node is the child of previous node in the list, and the parent of the next node in the list.
	def treed(cls, setfi_paths=None, setfo_paths=None):
		def treed_one_path(tree, path, flag):
			if not path:
				return;
			curr_node = path[0];
			del path[0];
			if flag:
				if tree.get(bydata={curr_node}):
					next_tree = tree.get(bydata={curr_node}).pop();
					treed_one_path(tree=next_tree,path=path,flag=flag);
				else:
					next_tree = tree.add(setfi={curr_node}).pop();
					treed_one_path(tree=next_tree,path=path,flag=flag);
			else:
				if tree.get(byid={curr_node}):
					next_tree = tree.get(byid={curr_node}).pop();
					treed_one_path(tree=next_tree,path=path,flag=flag);
				else:
					next_tree = tree.add(setfo={curr_node}).pop();
					treed_one_path(tree=next_tree,path=path,flag=flag);
			return;
		new_trees = set();
		if isinstance(setfi_paths, list):
			setfi_paths = [path for path in setfi_paths if isinstance(path, list)];
			for path in setfi_paths:
				tmp_root = Tree(setfo=new_trees);
				treed_one_path(tree=tmp_root,path=path,flag=True);
				tmp_root_childs = tmp_root.get();
				if not tmp_root_childs is None:
					new_trees |= tmp_root_childs;
		if isinstance(setfo_paths, list):
			setfo_paths = [[tree for tree in path if isinstance(tree, Tree)] for path in setfo_paths if isinstance(path, list) if path];
			for path in setfo_paths:
				tmp_root = Tree(setfo=new_trees);
				treed_one_path(tree=tmp_root,path=path,flag=False);
				tmp_root_childs = tmp_root.get();
				if not tmp_root_childs is None:
					new_trees |= tmp_root_childs;
		return new_trees;
	treed = classmethod(treed);


        ## @fn def __init__(self, data=None, setfo=None, setfi=None)
        # @brief Constructor. Potentially construct the children contained in \a setfi.
        # @author Sangsoic <sangsoic@protonmail.com>
        # @version 0.1
        # @date 2017-07-01 Wed 12:11 PM
        # @param self Instance pointer.
        # @param data Data to store in the node.
        # @param setfi Iterable object containing the value of the data of the child nodes to instantiate.
        # @param setfo Iterable object containing child nodes of the node being constructed.
	def __init__(self, data=None, setfo=None, setfi=None):
		self.data = data;
		self.__nodes = None;
		self.set(setfo, setfi); 

        ## @fn def __len__(self)
        # @brief Returns the number of children of the node.
        # @author Sangsoic <sangsoic@protonmail.com>
        # @version 0.1
        # @date 2017-07-02 Wed 1:15 PM
        # @param self Instance pointer.
        # @return Number of children of the node.
	def __len__(self):
		if self.__nodes is None:
			return 0;
		return len(self.__nodes);

        ## @fn def __str__(self)
        # @brief Defines the way a \a Tree object is represented when displayed by the interpreter.
        # @author Sangsoic <sangsoic@protonmail.com>
        # @version 0.1
        # @date 2017-07-02 Wed 1:19 PM
        # @param self Instance pointer.
        # @return Linear representation of a node.
	def __str__(self):
		return "Tree(%s -> %s)" % (self.data, self.get());
	__repr__ = __str__;

        ## @fn def __bool__(self)
        # @brief Defines what is returned when a \a Tree instance is tested.
        # @author Sangsoic <sangsoic@protonmail.com>
        # @version 0.1
        # @date 2017-07-02 Wed 1:25 PM
        # @param self Instance pointer.
        # @return False if the node has no children else True.
	def __bool__(self):
                return not self.__nodes is None;

        ## @fn def values(self)
        # @brief Generates recursively the data value of all the nodes of the tree.
        # @author Sangsoic <sangsoic@protonmail.com>
        # @version 0.1
        # @date 2017-07-02 Wed 1:31 PM
        # @param self Instance pointer.
        # @return The data value of a node.
	def values(self):
		for node in Tree.walk(tree=self):
			yield node.data;

        ## @fn def nodes(self)
        # @brief Generates recursively all the nodes of the tree.
        # @author Sangsoic <sangsoic@protonmail.com>
        # @version 0.1
        # @date 2017-07-02 Wed 1:37 PM
        # @param self Instance pointer.
        # @return A node.
	def nodes(self):
		for node in Tree.walk(tree=self):
			yield node;

        ## @fn def npaths(self)
        # @brief Generates recursively the path of all the nodes of the tree.
        # @author Sangsoic <sangsoic@protonmail.com>
        # @version 0.1
        # @date 2017-07-02 Wed 1:44 PM
        # @param self Instance pointer.
        # @return A path.
        # The path is composed the node themselves.
	def npaths(self):
		for path in Tree.walk(tree=self, path_recorder=[]):
			yield path;

        ## @fn def vpaths(self)
        # @brief Generates recursively the path of all the nodes of the tree.
        # @author Sangsoic <sangsoic@protonmail.com>
        # @version 0.1
        # @date 2017-07-02 Wed 1:52 PM
        # @param self Instance pointer.
        # @return A path.
        # The path is composed of the data value of the nodes.
	def vpaths(self):
		for path in Tree.walk(tree=self, path_recorder=[]):
			yield [node.data for node in path];
	__iter__ = vpaths;

        ## @fn def descendants_exists(self, setfi_paths=None, setfo_paths=None)
        # @brief Tests if the given paths exist within the tree.
        # @author Sangsoic <sangsoic@protonmail.com>
        # @version 0.1
        # @date 2017-07-02 Wed 1:58 PM
        # @param self Instance pointer.
        # @param setfi_paths List containing values corresponding to node data.
        # @param setfo_paths List containing \a Tree object instance.
        # @return True if all paths exist else False.
	def descendants_exists(self, setfi_paths=None, setfo_paths=None):
		return Tree.exists(tree=self, setfi_paths=setfi_paths, setfo_paths=setfo_paths);

        ## @fn def get(self, byidx=None, byid=None, bydata=None)
        # @brief Gets the set of all the direct children of this node. A specific selection by index, id, or data is possible.
        # @author Sangsoic <sangsoic@protonmail.com>
        # @version 0.1
        # @date 2017-07-02 Wed 2:10 PM
        # @param self Instance pointer.
        # @param byidx Only displays children nodes if their index is inside byidx.
        # @param byid Only displays children nodes if their id is inside byid.
        # @param bydata Only displays children nodes if their data is inside bydata.
        # @return The set of all the direct children of this node.
	def get(self, byidx=None, byid=None, bydata=None):
		found_childs = set();
		if self.__nodes is None:
			return None;
		if [byidx, byid, bydata] == 3*[None]:
			found_childs = self.__nodes;
			if not found_childs:
				return None;
			return found_childs.copy();
		if isinstance(byidx, Iterable):
			found_childs |= {list(self.__nodes)[idx] for idx in byidx if isinstance(idx, int) if idx >= 0 and idx < len(self.__nodes)};
		if isinstance(byid, Iterable):
			found_childs |= {tree for tree in byid if isinstance(tree, Tree) if tree in self.__nodes};
		if isinstance(bydata, Iterable):
			found_childs |= {tree for tree in self.__nodes if tree.data in bydata};
		if not found_childs:
			return None;
		return found_childs;

        ## @fn def set(self, setfo=None, setfi=None)
        # @brief Sets the direct children of the node.
        # @author Sangsoic <sangsoic@protonmail.com>
        # @version 0.1
        # @date 2017-07-02 Wed 2:20 PM
        # @param self Instance pointer.
        # @param setfo Iterable object containing child nodes.
        # @param setfi Iterable object containing the value of the data of the child nodes to instantiate.
        # @return The set of all the direct children of this node.
	def set(self, setfo=None, setfi=None):
		new_childs = set();
		if setfo is None and setfi is None:
			self.__nodes = None;
		else:
			if isinstance(setfo, Iterable):
				setfo = {tree for tree in setfo if isinstance(tree, Tree)};
				new_childs |= setfo;
			if isinstance(setfi, Iterable):
				setfi = {Tree(data=data) for data in setfi};
				new_childs |= setfi;
			if new_childs:
				self.__nodes = new_childs;
		if self.__nodes is None:
			return self.__nodes;
		return self.__nodes.copy();

        ## @fn def add(self, setfo=None, setfi=None)
        # @brief Adds direct children to the node.
        # @author Sangsoic <sangsoic@protonmail.com>
        # @version 0.1
        # @date 2017-07-02 Wed 2:50 PM
        # @param self Instance pointer.
        # @param setfo Iterable object containing child nodes.
        # @param setfi Iterable object containing the value of the data of the child nodes to instantiate.
        # @return The set of all the newly added children.
	def add(self, setfo=None, setfi=None):
		new_childs = set();
		if isinstance(setfo, Iterable):
			setfo = {tree for tree in setfo if isinstance(tree, Tree)};
			new_childs |= setfo;
		if isinstance(setfi, Iterable):
			setfi = {Tree(data=data) for data in setfi};
			new_childs |= setfi;
		if self.__nodes is None:
			if new_childs:
				self.__nodes = new_childs;
		else:
			self.__nodes |= new_childs;
		return new_childs.copy();

        ## @fn rem(self, byidx=None, byid=None, bydata=None)
        # @brief Removes children given by byidx, byid, or bydata.
        # @author Sangsoic <sangsoic@protonmail.com>
        # @version 0.1
        # @date 2017-07-02 Wed 2:35 PM
        # @param self Instance pointer.
        # @param byidx Only removes children nodes if their index is inside byidx.
        # @param byid Only removes children nodes if their id is inside byid.
        # @param bydata Only removes children nodes if their data is inside bydata.
        # @return The set of all the deleted children.
	def rem(self, byidx=None, byid=None, bydata=None):
		curr_target = None;
		childs_snapshot = None;
		deleted_target = None;
		if not self.__nodes is None:
			childs_snapshot = self.__nodes.copy();
			if isinstance(byidx, Iterable):
				curr_target = {list(self.__nodes)[idx] for idx in byidx if isinstance(idx, int) if idx >= 0 and idx < len(self.__nodes)};
				self.__nodes -= curr_target;
				if not self.__nodes:
					self.__nodes = None;
		if not self.__nodes is None:
			if isinstance(byid, Iterable):
				curr_target = {tree for tree in byid if isinstance(tree, Tree)};
				self.__nodes -= curr_target;
				if not self.__nodes:
					self.__nodes = None;
		if not self.__nodes is None:
			if isinstance(bydata, Iterable):
				curr_target = {tree for data in bydata for tree in self.__nodes if tree.data == data};
				self.__nodes -=  curr_target;
				if not self.__nodes:
					self.__nodes = None;
		if childs_snapshot is None:
			deleted_target = {};
		elif self.__nodes is None:
			deleted_target = childs_snapshot;
		else:
			deleted_target = childs_snapshot - self.__nodes;
		return deleted_target;

        ## @fn copy(self)
        # @brief Returns a copy of the Tree.
        # @author Sangsoic <sangsoic@protonmail.com>
        # @version 0.1
        # @date 2017-07-02 Wed 2:59 PM
        # @param self Instance pointer.
        # @return A copy of the Tree.
	def copy(self):
		self_copy = Tree.treed(setfi_paths=list(self));
		return self_copy.pop();
