# A simple python Tree object module.

*if you have any request regarding the project please contact the author at <sangsoic@protonmail.com>*

![Test in python interpreter](Img/test.png)

## About

This project consists of a simple Tree data structure in python. The object contains all necessary tools to convert a list of paths into trees and conversely trees into a list of paths.

* If you would like to know more about the general concept of trees [read this document](https://en.wikipedia.org/wiki/Tree_(data_structure)).
* Feel free to submit changes.

## Usage

This section covers essential notions. For more information, read the source code documentation for each routine.

### Instantiating.

Instantiating a Tree requires the data of the root node, along with the direct child nodes of the root.

There are two ways to specify what the direct children of root are during the construction.

1. Setting the children sub nodes from inside or **setfi**, which consist of passing the child nodes data value via the **setfi** parameter. Each element inside **setfi** will be a child of the root that we are currently instantiating.


```Python
t = Tree(data='/', setfi={'etc', 'dev', 'home', 'usr'})
>>> t = Tree(data='/', setfi={'etc', 'dev', 'home', 'usr'})
>>> t
Tree(/ -> {Tree(etc -> None), Tree(usr -> None), Tree(dev -> None), Tree(home -> None)})
```

2. Setting the children sub nodes from outside or setfo, which consist of passing previously instantiated nodes as the children to the current node, via the **setfo** parameter. Each element inside **setfo** will be a child of the root that we are currently instantiating.

```Python
>>> paths = [
... ['etc'],
... ['bin'],
... ['usr'],
... ['home'],
... ['mnt'],
... ['dev'],
... ['media'],
... ['sbin'],
... ['home', 'user0'],
... ['home', 'user1'],
... ['home', 'user2'],
... ['dev', 'block'],
... ['dev', 'bus'],
... ['dev', 'sda'],
... ['etc', 'shadow']
... ]
>>> childs = Tree.treed(paths) # This function takes paths and recursively create the tree which will contains each nodes of these paths.

>>> t = Tree(data='/', setfo=childs)
>>> t
Tree(/ -> {Tree(bin -> None), Tree(usr -> None), Tree(etc -> {Tree(shadow -> None)}), Tree(home -> {Tree(user2 -> None), Tree(user1 -> None), Tree(user0 -> None)}), Tree(sbin -> None), Tree(mnt -> None), Tree(media -> None), Tree(dev -> {Tree(block -> None), Tree(sda -> None), Tree(bus -> None)})})
```


The **data** parameter is used to set the data of the current root.

### Working with a node.

Several operations are doable on a simple node such as:

* Changing the node data.

```Python
>>> t = Tree(data='/')
>>> t
Tree(/ -> None)
>>> t.data = 'etc'
>>> t
Tree(etc -> None)
```

* Setting the node children.

```Python
>>> t = Tree('/')
>>> t
Tree(/ -> None)
>>> t.set(setfi={'dev', 'mnt', 'media', 'home'})
{Tree(dev -> None), Tree(mnt -> None), Tree(home -> None), Tree(media -> None)}
>>> t
Tree(/ -> {Tree(dev -> None), Tree(mnt -> None), Tree(home -> None), Tree(media -> None)})
```

* Getting the node children.

```Python
>>> t = Tree(data='/', setfi={'dev', 'mnt', 'media', 'home'})
>>> t.get()
{Tree(mnt -> None), Tree(media -> None), Tree(home -> None), Tree(dev -> None)}
>>> t.get(bydata={'mnt', 'dev'})
{Tree(mnt -> None), Tree(dev -> None)}
>>> t.get(byidx={1, 3, 0})
{Tree(mnt -> None), Tree(dev -> None), Tree(home -> None)}
>>> nodes = t.get(byidx={0, 1})
>>> nodes
{Tree(mnt -> None), Tree(dev -> None)}
>>> t.get(byid=nodes)
{Tree(mnt -> None), Tree(dev -> None)}
```

* Adding children to the node.

```Python
>>> t = Tree(data='/', setfi={'dev', 'mnt'})
>>> t
Tree(/ -> {Tree(dev -> None), Tree(mnt -> None)})
>>> t.add(setfi={'media', 'home'})
{Tree(home -> None), Tree(media -> None)}
>>> t
Tree(/ -> {Tree(home -> None), Tree(media -> None), Tree(dev -> None), Tree(mnt -> None)})
```

* Removing children from a node.

```Python
>>> t = Tree(data='/', setfi={'dev', 'mnt', 'media', 'home', 'usr', 'bin', 'sbin', 'etc'})
>>> t.rem(bydata={'dev', 'mnt', 'home'})
{Tree(mnt -> None), Tree(home -> None), Tree(dev -> None)}
>>> t
Tree(/ -> {Tree(usr -> None), Tree(etc -> None), Tree(bin -> None), Tree(media -> None), Tree(sbin -> None)})
>>> t.rem(byidx={0, 1, 2})
{Tree(etc -> None), Tree(media -> None), Tree(sbin -> None)}
>>> t
Tree(/ -> {Tree(usr -> None), Tree(bin -> None)})
>>> nodes = t.get(bydata={'usr'})
>>> nodes
{Tree(usr -> None)}
>>> t.rem(byid=nodes)
{Tree(usr -> None)}
>>> t
Tree(/ -> {Tree(bin -> None)})
```

### Working with sub nodes recursively.

It is possible to perform global recursive operations on a Tree such as:


* Converting a list of paths to a tree.

```Python
>>> paths = [
... ['etc'],
... ['bin'],
... ['usr'],
... ['home'],
... ['mnt'],
... ['dev'],
... ['media'],
... ['sbin'],
... ['home', 'user0'],
... ['home', 'user1'],
... ['home', 'user2'],
... ['dev', 'block'],
... ['dev', 'bus'],
... ['dev', 'sda'],
... ['etc', 'shadow']
... ]
>>> forest = Tree.treed(paths)
{Tree(bin -> None), Tree(usr -> None), Tree(etc -> {Tree(shadow -> None)}), Tree(home -> {Tree(user2 -> None), Tree(user1 -> None), Tree(user0 -> None)}), Tree(sbin -> None), Tree(mnt -> None), Tree(media -> None), Tree(dev -> {Tree(block -> None), Tree(sda -> None), Tree(bus -> None)})}
```

* Testing the existence of a list of paths.

Following the first example.

```Python
>>> t = Tree(data='/', setfo=forest)
>>> Tree.tests(t, [['/', 'dev', 'bus'], ['/', 'etc']])
True
```

* Walking through a Tree.

Following the first example.

```Python
>>> for valuePath in t.vpaths():
...     print(valuePath)
... 
['/']
['/', 'dev']
['/', 'dev', 'sda']
['/', 'dev', 'block']
['/', 'dev', 'bus']
['/', 'bin']
['/', 'media']
['/', 'usr']
['/', 'sbin']
['/', 'home']
['/', 'home', 'user1']
['/', 'home', 'user2']
['/', 'home', 'user0']
['/', 'mnt']
['/', 'etc']
['/', 'etc', 'shadow']
>>> for node in t.nodes():
...     print(node)
... 
Tree(/ -> {Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(bin -> None), Tree(media -> None), Tree(usr -> None), Tree(sbin -> None), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(mnt -> None), Tree(etc -> {Tree(shadow -> None)})})
Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)})
Tree(sda -> None)
Tree(block -> None)
Tree(bus -> None)
Tree(bin -> None)
Tree(media -> None)
Tree(usr -> None)
Tree(sbin -> None)
Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)})
Tree(user1 -> None)
Tree(user2 -> None)
Tree(user0 -> None)
Tree(mnt -> None)
Tree(etc -> {Tree(shadow -> None)})
Tree(shadow -> None)
>>> for value in t.values():
...     print(value)
... 
/
dev
sda
block
bus
bin
media
usr
sbin
home
user1
user2
user0
mnt
etc
shadow
>>> for nodePath in t.npaths():
...     print(nodePath)
... 
[Tree(/ -> {Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(bin -> None), Tree(media -> None), Tree(usr -> None), Tree(sbin -> None), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(mnt -> None), Tree(etc -> {Tree(shadow -> None)})})]
[Tree(/ -> {Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(bin -> None), Tree(media -> None), Tree(usr -> None), Tree(sbin -> None), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(mnt -> None), Tree(etc -> {Tree(shadow -> None)})}), Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)})]
[Tree(/ -> {Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(bin -> None), Tree(media -> None), Tree(usr -> None), Tree(sbin -> None), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(mnt -> None), Tree(etc -> {Tree(shadow -> None)})}), Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(sda -> None)]
[Tree(/ -> {Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(bin -> None), Tree(media -> None), Tree(usr -> None), Tree(sbin -> None), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(mnt -> None), Tree(etc -> {Tree(shadow -> None)})}), Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(block -> None)]
[Tree(/ -> {Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(bin -> None), Tree(media -> None), Tree(usr -> None), Tree(sbin -> None), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(mnt -> None), Tree(etc -> {Tree(shadow -> None)})}), Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(bus -> None)]
[Tree(/ -> {Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(bin -> None), Tree(media -> None), Tree(usr -> None), Tree(sbin -> None), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(mnt -> None), Tree(etc -> {Tree(shadow -> None)})}), Tree(bin -> None)]
[Tree(/ -> {Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(bin -> None), Tree(media -> None), Tree(usr -> None), Tree(sbin -> None), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(mnt -> None), Tree(etc -> {Tree(shadow -> None)})}), Tree(media -> None)]
[Tree(/ -> {Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(bin -> None), Tree(media -> None), Tree(usr -> None), Tree(sbin -> None), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(mnt -> None), Tree(etc -> {Tree(shadow -> None)})}), Tree(usr -> None)]
[Tree(/ -> {Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(bin -> None), Tree(media -> None), Tree(usr -> None), Tree(sbin -> None), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(mnt -> None), Tree(etc -> {Tree(shadow -> None)})}), Tree(sbin -> None)]
[Tree(/ -> {Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(bin -> None), Tree(media -> None), Tree(usr -> None), Tree(sbin -> None), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(mnt -> None), Tree(etc -> {Tree(shadow -> None)})}), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)})]
[Tree(/ -> {Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(bin -> None), Tree(media -> None), Tree(usr -> None), Tree(sbin -> None), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(mnt -> None), Tree(etc -> {Tree(shadow -> None)})}), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(user1 -> None)]
[Tree(/ -> {Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(bin -> None), Tree(media -> None), Tree(usr -> None), Tree(sbin -> None), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(mnt -> None), Tree(etc -> {Tree(shadow -> None)})}), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(user2 -> None)]
[Tree(/ -> {Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(bin -> None), Tree(media -> None), Tree(usr -> None), Tree(sbin -> None), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(mnt -> None), Tree(etc -> {Tree(shadow -> None)})}), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(user0 -> None)]
[Tree(/ -> {Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(bin -> None), Tree(media -> None), Tree(usr -> None), Tree(sbin -> None), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(mnt -> None), Tree(etc -> {Tree(shadow -> None)})}), Tree(mnt -> None)]
[Tree(/ -> {Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(bin -> None), Tree(media -> None), Tree(usr -> None), Tree(sbin -> None), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(mnt -> None), Tree(etc -> {Tree(shadow -> None)})}), Tree(etc -> {Tree(shadow -> None)})]
[Tree(/ -> {Tree(dev -> {Tree(sda -> None), Tree(block -> None), Tree(bus -> None)}), Tree(bin -> None), Tree(media -> None), Tree(usr -> None), Tree(sbin -> None), Tree(home -> {Tree(user1 -> None), Tree(user2 -> None), Tree(user0 -> None)}), Tree(mnt -> None), Tree(etc -> {Tree(shadow -> None)})}), Tree(etc -> {Tree(shadow -> None)}), Tree(shadow -> None)]
>>> for valuePath in t:
...     print(valuePath)
... 
['/']
['/', 'dev']
['/', 'dev', 'sda']
['/', 'dev', 'block']
['/', 'dev', 'bus']
['/', 'bin']
['/', 'media']
['/', 'usr']
['/', 'sbin']
['/', 'home']
['/', 'home', 'user1']
['/', 'home', 'user2']
['/', 'home', 'user0']
['/', 'mnt']
['/', 'etc']
['/', 'etc', 'shadow']
```

* Copying a Tree.

```Python
>>> t = Tree(data='/', setfi={'dev', 'mnt', 'media', 'home'})
>>> copy = t.copy()
>>> copy
Tree(/ -> {Tree(dev -> None), Tree(mnt -> None), Tree(media -> None), Tree(home -> None)})
```

### Additional.

For even more functionalities you can read the source code documentation present above each routine header.
